import React from 'react';
import Routes from './components/Routes';
import Navbar from './components/Navbar';

// no need to declare App as a Component since it's just for rendering purposes
const App = () => (
  <div>
     <Navbar />
    <div className="container">
      <Routes />
    </div>
  </div>
)

export default App;
