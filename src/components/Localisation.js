import React from 'react';
import {geolocated} from 'react-geolocated';
import ShopsList from './ShopsList';

class Localisation extends React.Component {
  render() {
    return !this.props.isGeolocationAvailable
      ? <h1>Your browser does not support Geolocation</h1>
      : !this.props.isGeolocationEnabled
        ? <h1>Geolocation is not enabled</h1>
        : this.props.coords
          ? <ShopsList latitude={this.props.coords.latitude} longitude={this.props.coords.longitude}/>
          : <h1>Getting the location data&hellip; </h1>;
  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: true,
  },
  userDecisionTimeout: 5000,
})(Localisation);