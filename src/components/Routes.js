import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Localisation from './Localisation';


const Routes = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Localisation} />
    </Switch>
  </main>
)

export default Routes;