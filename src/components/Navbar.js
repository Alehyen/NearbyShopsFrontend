import React from 'react';
import { Link } from 'react-router-dom';



const Navbar = () => (
  
      <div>
        <nav className="blue darken-3">
            <div className="nav-wrapper">
            <a  className="brand-logo">  Nearby Shops</a>
           <a data-activates="main-menu" className="button-collapse show-on-small">
              <i className="fa fa-bars"></i>
           </a>
            <ul className="right hide-on-med-and-down">
                <li><Link to="/"><i className="fa fa-map-marker"></i>  Nearby Shops</Link></li>
            </ul>
            <ul className="side-nav" id="main-menu">
                <li><Link to="/"><i className="fa fa-map-marker"></i>  Nearby Shops</Link></li>
            </ul>
            </div>
        </nav>
   </div>
)

export default Navbar;