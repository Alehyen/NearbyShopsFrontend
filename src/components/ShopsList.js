import React, { Component } from 'react';
import axios from 'axios';
import BASE_URL from '../shared';
import Shop from './Shop';

// this is the root component, it's responsible for rendering the list of the nearby shops
class ShopsList extends Component {
    constructor() {
        super();
        this.state = {
            shops: [],
            likedshops:[],
        }
    }

    componentWillMount(){
        this.getShopsList(this.props.latitude,this.props.longitude);
    }

    // get the list of all the shops in the database
    getShopsList(latitude,longitude) {
        axios.get(BASE_URL + 'shops/',{
            params:{
                latitude:latitude,
                longitude:longitude,
            }
        })
            .then(response => {
                this.setState({ shops: response.data }, () => {
                    console.log("shops list updated");
                })
            })
            .catch(err => console.log(err));
    }

// if the user click on the like button this function will be called
// we push the shop into the liked shops array and we pull it from the normal shops array
    like(index){
        const shops = this.state.shops.slice();
        const likedshopstemp = this.state.likedshops.slice();
         likedshopstemp.push(shops[index]);
        shops.splice(index,1);
        this.setState({
            shops:shops,
            likedshops:likedshopstemp,
        });
      
    }
    //dislike a shop. 
dislike(index){
        console.log("dislike")
       
        const shopstemp = this.state.shops.slice();
        // the temporary shop to be removed and added again 2 hous later
        const tempshop= shopstemp[index];
        shopstemp.splice(index,1);
        this.setState({
            shops:shopstemp,
        },()=>{
            setTimeout(function() {
             const shopstemp = this.state.shops.slice();
             shopstemp.splice(index,0,tempshop);
             this.setState({
                shops:shopstemp,
             });
              }.bind(this),7200000);
          });
        
    }
    //remove a shop from liked shops list
remove(index){
        const shopsliked = this.state.likedshops.slice();
        shopsliked.splice(index,1);
        this.setState({
            likedshops:shopsliked,
        });
    }
    
    render() {

        //avoid mutability
        const shopslist = this.state.shops.map((shop, i) => {
            return (
            <Shop key={shop._id} item={shop} liked={false} dislike={() => this.dislike(i)} like={() => this.like(i)}  />
            )
        })
         const likedshopsList = this.state.likedshops.map((shop, i) => {
            return (     
            <Shop key={shop._id} item={shop} liked={true} remove={() => this.remove(i)}  /> 
            )
        })
        return (
            <div>
                 <h1 className="center">My preferred Shops</h1>
                 <div className="row">
                {likedshopsList.length ? likedshopsList : 'No Preferred Shops found!' }
                 </div>
                <h1 className="center">Nearby Shops</h1>
                 <div className="row">
                      {shopslist.length ? shopslist : 'Sorry!, No Shops Found Near You'}
                </div>
            </div>
        )
    }
}

export default ShopsList;