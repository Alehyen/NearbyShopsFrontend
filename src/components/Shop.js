import React, { Component } from 'react';

// Shop component contain the UI for rendring a single shop.
// the shop information are passed through props  from the parent component (ShopsList) and stored into the state.
class Shop extends Component{

constructor(props){
    super(props);
    this.state = {
      item: this.props.item,
      liked: false,
    }
  }

 componentWillMount(){
      this.setState({
          liked:this.props.liked,
      });
  }

  render(){
    return (
        <div className="col l4 m6 s12">
          <div className="card small">
            <div className="card-image">
              <img src="http://materializecss.com/images/sample-1.jpg" alt="" />
              <span className="card-title">{this.state.item.name} | {this.state.item.city}</span>
            </div>
            <div className="card-action">
                {this.state.liked ? <LikedShopActions remove={this.props.remove}  /> : <NormalShopActions dislike={this.props.dislike} like={this.props.like} /> }
            </div>
          </div>
        </div>
    )
  }

}

//  liked shops  can be removed from the liked shops list
      function LikedShopActions(props) {
      return ( <button className="btn red" onClick={props.remove}>Remove</button>);
  }
  

  // normal shops can be liked or disliked
   function NormalShopActions(props){
      return (
          <div>
            <button className="btn red" onClick={props.dislike}>Dislike</button>
            <button className="btn green" onClick={props.like}>Like</button>
          </div>
      );
  }

export default Shop;