# Frontend of the nearby shops challenge

I'll be using ReactJS.


# Run the Project

```npm install && npm start```   and Enjoy! :smiley:

:warning: ``` make sure the MongoDB server and the Django App is running (you should run the django app with the command python manage.py 0.0.0.0:8000), and in the shared.js file in the root folder of the react App change the BASE_URL to your IP Adress to be able to communicate with the Django App ```

# How it Works!
the app is going to ask your permission to share your position, once shared you will get the list of the nearby shops in 2 KM radius
Then you can like a shop (it will be added to the Preferred shops list and removed from the nearby shops list), dislike a shop (it will be removed from the nearby shops list for 2 hours),
and you can remove a shop from the Preferred shops list.

### That's it guys Hope you like it. :wink:

